// Essential Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>

// Libraries used in sockets
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <sys/signal.h>
#include <arpa/inet.h>
#include <netdb.h>

#define BUFFSIZE 4096
#define PORT 2000

struct sockaddr_in remote;

int socketServer;
int serverSec = sizeof(remote);

char buffer[BUFFSIZE];

int mathOperator;
int value1, value2, result, recieve, flag = 0;

int main()
{
    system("clear");
    printf("Copyright -> 2020 Made by Wendel Schimitz\n");
    printf("---------------------- CLIENT ------------------\n");
    printf("1) Addition\n2) Subtraction\n3) Multiplication\n4) Division\n5) Exp\n6) Square Root\n");
    socketServer = socket(AF_INET, SOCK_STREAM, 0);

    if(socketServer == -1)
    {
        perror("socket error");
        exit(1);
    }

    remote.sin_family = AF_INET;
    remote.sin_port   = htons(PORT);
    remote.sin_addr.s_addr  = inet_addr("127.0.0.1");

    if(connect(socketServer, (struct sockaddr*)&remote, serverSec) == -1)
    {
        perror("connect error");
        exit(1);
    }
    
    printf("Choose an operation option: ");
    scanf("%d", &mathOperator);
    if(mathOperator == 4){
        printf("Enter the first value: ");
        scanf("%d",&value1);
        printf("Enter the second value: ");
        scanf("%d",&value2);
        
        while(value2 == 0){
            printf("Impossible to devide by zero. Try again.\n");
            printf("Enter the second value again: ");
            scanf("%d",&value2);
        } 
    }
    else if(mathOperator == 6) // Raiz quadrada de um valor
    {
        printf("Digite o valor: ");
        scanf("%d",&value1);
    }
    else
    {
        printf("Enter the first value: ");
        scanf("%d",&value1);
        printf("Enter the second value: ");
        scanf("%d",&value2);
    }
    
    printf("\n");

    if (send(socketServer, &value1, sizeof(int), flag) == -1 ) 
    {
        perror("send value 1 error");  
        close(socketServer);
        exit(1);
    }else{
        system("clear");
        printf("Copyright -> 2020 Made by Wendel Schimitz\n\n\n");
        printf("Values sent to Server.\n");
    }
    if (send(socketServer, &value2, sizeof(int), flag) == -1) {
        perror("send value 2 error"); 
        close(socketServer);
        exit(1);
    }

    if (send(socketServer, &mathOperator, sizeof(int), flag) == -1) {
        perror("send math operator error"); 
        close(socketServer);
        exit(1);
    }
    printf("\n");

    //Var who receive the result from client
    recieve = recv(socketServer, &result, sizeof(int), flag);    
        
    if (recieve == -1) { 
        perror("falha no recebimento do resultado");
        close(socketServer);
        exit(1);
    }else{ 
        printf("===> Result: %d\n", result);
        printf("\n\n> > > Client Closed. < < <\n");
        close(socketServer);
        exit(1);
    }

    return 0;
}