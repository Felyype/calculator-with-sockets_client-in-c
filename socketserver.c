// Essential Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>

// Libraries used in sockets
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <sys/signal.h>
#include <arpa/inet.h>
#include <netdb.h>

#define BUFFSIZE 4096
#define PORT 2000

struct sockaddr_in local;
struct sockaddr_in remote;

int socketServer;
int clientMain;

char buffer[BUFFSIZE];

int mathOperator, Operatoraux;
int result, flag = 0;
int value1, value2, value1aux, value2aux; 

int main()
{
    local.sin_family = AF_INET;
    local.sin_port   = htons(PORT);

    socketServer = socket(AF_INET, SOCK_STREAM, 0);

    if(socketServer == -1)
    {
        perror("socket error");
        exit(1);
    }
    else printf("SERVER ----------->\nSuccess in creating socket!\n");

    if(bind(socketServer,(struct sockaddr*)&local, sizeof(local)) == -1)
    {
        perror("bind error");
        exit(1);
    }

    listen(socketServer, 2);

    int *newsocketfd = (int *) malloc(sizeof(newsocketfd));
    int clien_leng = sizeof(newsocketfd);

    if((*newsocketfd = accept(socketServer, (struct sockaddr*)&remote, &clien_leng)) == -1)
    {
        perror("accept error");
        exit(1);
    }

    strcpy(buffer, "Welcome!!! \n\0");

    if(send(clientMain, buffer, strlen(buffer), 0))
    {
        printf("Waiting for client....\n");

        while (1)
        {
            if(recv(*newsocketfd, &value1, sizeof(int), flag) == -1)
            {
                perror("Not received value 1");
                exit(1);
            }
            else value1aux = value1; 
            
            if(recv(*newsocketfd, &value2, sizeof(int), flag) == -1)
            {
                perror("Not received value 2");
                exit(1);
            }else value2aux = value2; 

            if(recv(*newsocketfd, &mathOperator, sizeof(int), flag) == -1)
            {
                perror("Not received operator");
                exit(1);
            }else{
                Operatoraux = mathOperator;
                if(Operatoraux == 1) printf("Addition Operation.\n");
                else if(Operatoraux == 2) printf("Subtraction Operation.\n");
                else if(Operatoraux == 3) printf("Multilication Operation.\n");
                else if(Operatoraux == 4) printf("Division Operation.\n");
                else if(Operatoraux == 5) printf("Exp Operation.\n");
            } 
            
            switch (mathOperator)
            {
            case 1:
                result = value1 + value2;
                printf("Addition: %d + %d = %d", value1, value2, result);
                send(*newsocketfd, &result, sizeof(result), flag);
                break;
            
            case 2:
                result = value1 - value2;
                printf("Subtraction: %d - %d = %d", value1, value2, result);
                send(*newsocketfd, &result, sizeof(result), flag);
                break;
            case 3:
                result = value1 * value2;
                printf("Multiplication: %d * %d = %d", value1, value2, result);
                send(*newsocketfd, &result, sizeof(result), flag);
                break;
            case 4:
                result = value1 / value2;
                printf("Division: %d / %d = %d", value1, value2, result);
                send(*newsocketfd, &result, sizeof(result), flag);
                break;
            case 5:
                result = pow ((double)value1, (double)value2);
                printf("Exp: %d ^ %d = %d", value1, value2, result);
                send(*newsocketfd, &result, sizeof(result), flag);
                break;
            case 6:
                result = sqrt((double)value1);
                printf("Square Root: %d = %.1f\n",value1, (double)result);
                send(*newsocketfd, &result, sizeof(result), flag);
                break;
            default:
                printf("Invalid option.\n");
                break;
            }

            close(clientMain);
            break;
        }
    } 
    close(socketServer);
    printf("\nServer closed..\n");
    
    return 0;
}
